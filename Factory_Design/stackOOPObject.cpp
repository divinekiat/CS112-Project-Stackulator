#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

class StackulatorOOP : public Stackulator {
	public:
		// Node structure
		struct Node {
			string data;
			Node* next;
		};

		Node* top;
		// constructor
		StackulatorOOP() {
			top = NULL;
		}

		// checks if stack is empty
		bool isEmpty() {
			return top == NULL;
		}

		// adds data on top of the stack
		void push(string data) {
			Node* n = new Node;
			n->data = data;
			n->next = NULL;

			if(top != NULL)
				n->next = top;

			top = n;
		}

		// removes the top of the stack and returns its data
		string pop() {
			if(isEmpty()) {
				cerr << "Empty stack, nothing to pop\n";
				return "empty";
			} else {
				Node* temp = top;
				string tempData = temp->data;
				top = top->next;
				delete(temp);
				return tempData;
			}
		}

		// displays the stack from top to bottom
		void show() {
			for (Node *n=top; n != NULL; n=n->next)
	        	cout << n->data << endl;
		}


		// checks if string is a number
		bool isNumber(string s) {
			char* cs;
			strtod(s.c_str(), &cs);
			return *cs == 0;
		}

		// checks if string is an operator
		bool isOperator(string s) {
			char o = s[0];
			return (o == '+' || o == '-' || o == '/' || o == 'x');
		}

		// evaluates and returns a string of the answer
		string evaluate(double op1, double op2, char op) {
			double ans;
			switch(op) {
				case '+':
					ans = op1 + op2;
					break;
				case '-':
					ans = op1 - op2;
					break;
				case 'x':
					ans = op1 * op2;
					break;
				case '/':
					ans = op1 / op2;
					break;
			}

			ostringstream strAns;
			strAns << ans;

			return strAns.str();
		}

		// calculates
		double calculate(int argc, char** argv) {
			 StackulatorOOP s = StackulatorOOP();

			 // post-fix notation algorithm
		    for(size_t i = 0; i < argc; i++) {
		    	if(isNumber(argv[i])) {
		    		s.push(argv[i]);
		    	} else if(isOperator(argv[i])) {
		    		s.push(argv[i]);
		    		char op = (s.pop())[0];
		    		double op1 = atof(s.pop().c_str());
		    		double op2 = atof(s.pop().c_str());

		    		s.push(evaluate(op2, op1, op));
		    	}
		    }

		    // converts answer from string to double
		    double answer = atof(s.pop().c_str());

			return answer;
		}
};