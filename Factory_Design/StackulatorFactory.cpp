#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "stackImpObject.cpp"
#include "stackOOPObject.cpp"

using namespace std;

int main(int argc, char** argv) {
	
    
    Stackulator *st;
    // number of calculators to be manufactured
    int numOfCalc = stof(argv[1]);

    // size of array of input
    int size = argc - (2 + numOfCalc);
    char* input[size];
    int inputSize = 0;

    // loop creation of classes
    for( int i = 0; i < numOfCalc; i++ )
    {
        string type = (string) argv[2 + i];
        if (type == "Imperative") {
            st = new StackulatorImperative(); // creates new imperative stackulator
        } else if (type == "OOP") {
            st = new StackulatorOOP(); // creates new OOP stackulator
        }

        for (int j = (2 + numOfCalc); j < argc; j++) {
            input[inputSize] = argv[j];
            inputSize++;
        }

        double result = st->calculate(size, input);
        cout << "Result for " << argv[2+i] << " implemented calculator = " << result << endl;
    }

	return 0;
}