A FACTORY 🏭 of 2 types of Calculator Objects 🔢🔢🔢

A FACTORY OBJECT that is able to create instances of calculator objects that work based on the Stack data structure. The calculator instances that can be produced by the factory should be implemented in two separate ways: 

1. **A working calculator program, coded imperatively.**
    - the stack data structure is coded NOT using object-oriented principles (i.e. a purely array based implementation of the stack data structure)

2. **A working calculator program, coded using OOP**
    - the stack data structure is coded using object-oriented principles (i.e. use of node objects and linked-lists)

### HOW TO COMPILE

```
g++ -o StackulatorFactory StackulatorFactory.cpp
```

#### How to Run:
```
StackulatorFactory <Number of Calculators> <Types separated with space> 5 3 + 
```
##### Example:
```
StackulatorFactory 3 Imperative OOP Imperative 5 3 +
```

Types can be:
- `OOP`
- `Imperative`

Accepted operators:
- `+`
- `-`
- `x`
- `/`

> **Important note:** Multiplication is `x` instead of `*` to avoid command line complications.

**Authors:**
- John Raphael Faustino
- Divine-kia Tan