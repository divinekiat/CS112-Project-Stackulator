// Stackulator abstract class
class Stackulator {

    public:
        virtual double calculate ( int argc, char** input ) = 0;
};