#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "Stackulator.cpp"

using namespace std;

class StackulatorImperative : public Stackulator
{
	public:
		double calculate ( int argc, char** argv )
		{
			string stack[argc];
			int top = -1;
			double result;

			for (int i = 0; i < argc; i++)
			{
				string in = argv[i];
				char* cs;
				strtod(in.c_str(), &cs);
				
				// If top is an operand
				if (*cs == 0)
				{
					top++;
					stack[top] = in;
				}
				// If top is an operator
				else
				{	
					// The number of operators is at least the same as operands, if not more
					if (top - 1 < 0)
					{
						cout << "Empty stack, nothing to pop" << endl;

						return 0;
					}
					else
					{
						top++;
						stack[top] = in;

						char op = (stack[top])[0];
						double op2 = atof(stack[top - 1].c_str());
						double op1 = atof(stack[top - 2].c_str());

						// switches according to the operator
						switch (op)
						{
							case '+':
								result = op1 + op2;
								break;
							case '-':
								result = op1 - op2;
								break;
							case 'x':
								result = op1 * op2;
								break;
							case '/':
								result = op1 / op2;
								break;
						}

						top = top - 2;
						stack[top] = to_string(result);
					}
				}

			}

			return result;
		}
};