A working calculator program, coded using OOP
- the stack data structure is coded using object-oriented principles (i.e. use of node objects and linked-lists)

HOW TO RUN

Compile:
g++ -o stackulatorOOP stackulatorOOP.cpp

Run:
stackulatorOOP 5 3 +

Accepted operators:
- +
- -
- x
- /

Important note: Multiplication is "x" instead of "*" to avoid command line complications.

Authors:
- John Raphael Faustino
- Divine-kia Tan