A working calculator program, coded imperatively.
- in this implementation, the stack data structure is coded NOT using object-oriented
principles (i.e. a purely array based implementation of the stack data structure). 

HOW TO RUN

Compile:
g++ -o stackulatorImperative stackulatorImperative.cpp

Run:
stackulatorImperative 5 3 + 

Accepted operators:
- +
- -
- x
- /

Important note: Multiplication is "x" instead of "*" to avoid command line complications.

Authors:
- John Raphael Faustino
- Divine-kia Tan