A calculator 🔢 that works based on the Stack data structure, implemented in two separate ways:

1. **A working calculator program, coded imperatively.**
    - the stack data structure is coded NOT using object-oriented principles (i.e. a purely array based implementation of the stack data structure)

2. **A working calculator program, coded using OOP**
    - the stack data structure is coded using object-oriented principles (i.e. use of node objects and linked-lists)

### IMPERATIVE: HOW TO RUN

##### Compile:
```
g++ -o stackulatorImperative stackulateorImperative.cpp
```

#### Run:
```
stackulatorImperative 5 3 + 
```

### OOP: HOW TO RUN

##### Compile:
```
g++ -o stackulatorOOP stackulatorOOP.cpp
```

#### Run:
```
stackulatorOOP 5 3 + 
```

Accepted operators:
- `+`
- `-`
- `x`
- `/`

> **Important note:** Multiplication is `x` instead of `*` to avoid command line complications.

**Authors:**
- John Raphael Faustino
- Divine-kia Tan