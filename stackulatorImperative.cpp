#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char** argv) {
	string stack[argc - 1];
	int top = -1;
	double result;

	for (int i = 1; i < argc; i++)
	{
		string in = argv[i];
		char* cs;
		strtod(in.c_str(), &cs);
		
		// If top is an operand
		if (*cs == 0)
		{
			top++;
			stack[top] = in;
		}
		// If top is an operator
		else
		{	
			// The number of operators is at least the same as operands, if not more
			if (top - 1 < 0)
			{
				cout << "Empty stack, nothing to pop" << endl;

				return 0;
			}
			else
			{
				top++;
				stack[top] = in;

				char op = (stack[top])[0];
				double op2 = atof(stack[top - 1].c_str());
				double op1 = atof(stack[top - 2].c_str());

				switch (op)
				{
					case '+':
						result = op1 + op2;
						break;
					case '-':
						result = op1 - op2;
						break;
					case 'x':
						result = op1 * op2;
						break;
					case '/':
						result = op1 / op2;
						break;
				}

				top = top - 2;
				stack[top] = to_string(result);
			}
		}

	}

	cout << result << endl;

	return 0;
}